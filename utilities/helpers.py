import logging
import time
import calendar

import requests


def generate_sms_code(iccid):
    url = f'http://10.83.9.27:8888/api/sso-keycloak/otp/generate/{iccid}'
    response = requests.request("GET", url)
    return response.text


def stop_and_wait(driver, number):
    time.sleep(number)
    driver.implicitly_wait(number * 10)


def get_timestamp():
    return calendar.timegm(time.gmtime())


def snip_screen(driver):
    driver.save_screenshot(f'.//Screenshots/{get_timestamp()}_example_screenshot_fail.png')


def generate_log():
    logging.basicConfig(filename=".//Logs/autotest.log",
                        format='%(time)s: %(levels)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger
