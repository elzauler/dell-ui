import configparser

config = configparser.RawConfigParser()
config.read(".//Configurations/config.ini")


class ReadConfig:
    @staticmethod
    def get_url():
        url = config.get('common info', 'base_url')
        return url

    @staticmethod
    def get_iccid():
        iccid = config.get('common info', 'testing_iccid')
        return iccid
