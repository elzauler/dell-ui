import utilities.helpers as helpers
from pageObjects.LoginPage import LoginPage
from utilities.readProperties import ReadConfig


class TestLoginPage:
    URL = ReadConfig.get_url()
    ICCID = ReadConfig.get_iccid()

    def test_verify_login_page(self, setup):
        self.driver = setup
        self.driver.get(self.URL)

        helpers.stop_and_wait(self.driver, 3)

        if "Welcome back" in self.driver.page_source:
            assert True
        else:
            helpers.snip_screen(self.driver)
            assert False

    def test_valid_login(self, setup):
        self.driver = setup
        self.driver.get(self.URL)
        page_exe = LoginPage(self.driver)

        helpers.stop_and_wait(self.driver, 1)

        page_exe.set_iccid(self.ICCID)
        page_exe.click_sign_in_button()

        helpers.stop_and_wait(self.driver, 1)

        page_exe.set_sms_code(helpers.generate_sms_code(self.ICCID))
        page_exe.click_sign_in_button()

        helpers.stop_and_wait(self.driver, 5)

        if "dashboard" in self.driver.current_url and self.ICCID in self.driver.page_source:
            assert True
        else:
            helpers.snip_screen(self.driver)
            assert False

    def test_invalid_login(self, setup):
        self.driver = setup
        self.driver.get(self.URL)
        page_exe = LoginPage(self.driver)

        page_exe.set_iccid(self.ICCID)
        page_exe.click_sign_in_button()

        page_exe.set_sms_code(helpers.generate_sms_code(self.ICCID)[::-1])
        page_exe.click_sign_in_button()

        helpers.stop_and_wait(self.driver, 1)

        if "dashboard" in self.driver.current_url or self.ICCID in self.driver.page_source:
            helpers.snip_screen(self.driver)
            assert False
        else:
            assert True
