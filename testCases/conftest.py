import pytest
from selenium import webdriver


@pytest.fixture(scope='function')
def setup():
    print("initiating chrome DRIVER")
    driver = webdriver.Chrome(".//drivers/chromedriver.exe")
    driver.implicitly_wait(30)
    driver.maximize_window()

    yield driver
    driver.close()
