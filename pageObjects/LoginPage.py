class LoginPage:
    input_iccid = "//*[@id='input-iccid']"
    input_enterCode = "//input[contains(@id, 'otp_0_')]"
    button_signIn = "//*[@id='button-sign-in']"
    overlay_error = "//*[@id='cdk-overlay-0']/snack-bar-container"

    def __init__(self, driver):
        self.driver = driver

    def set_iccid(self, iccid):
        self.driver.find_element_by_xpath(self.input_iccid).send_keys(iccid)

    def set_sms_code(self, sms_code):
        self.driver.find_element_by_xpath(self.input_enterCode).send_keys(sms_code)

    def is_overlay_error_appear(self):
        return self.driver.find_element_by_xpath(self.overlay_error).isDisplayed()

    def click_sign_in_button(self):
        self.driver.find_element_by_xpath(self.button_signIn).click()
