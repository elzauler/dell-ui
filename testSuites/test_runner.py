import unittest

from testCases.test_login import LoginTests


# Get all tests from the test classes
testLoader = unittest.TestLoader()

sanity = [
    testLoader.loadTestsFromTestCase(LoginTests)
]

# Create a test suite combining all test classes
unittest.TestSuite(sanity)
# regressionTest = unittest.TestSuite([tc2, tc1])
